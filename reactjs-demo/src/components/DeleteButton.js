import React, {Component} from 'react';
import { Button } from 'reactstrap';

class DeleteButton extends Component{


    render() {
        return(
            <div>
                <Button id={this.props.id} onClick={this.props.callFunction} color="danger" size='sm'>Delete</Button>
            </div>
        )
    }

}

export default DeleteButton;