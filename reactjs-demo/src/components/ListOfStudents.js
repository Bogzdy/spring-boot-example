import React, {Component} from "react";
import AppNav from "./AppNav";
import {Table} from "reactstrap";
import InputTxtStudent from "./InputTxtStudent";
import CoolButton from "./CoolButton";
import DeleteButton from "./DeleteButton";

class ListOfStudents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            students: [],
            keys: [],
            nameValue: '',
            emailValue: '',
            sentStudent: {
                name: '',
                email: ''
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);

    }


    async componentDidMount() {
        const response = await fetch('/api/student');
        const body = await response.json();
        this.setState({students: body, isLoading: false, keys: Object.keys(body[0])})
    }

    handleNameChange = (e) =>{
        this.setState({
            nameValue: e.target.value
        });
        console.log(this.state.nameValue)
    };

    handleEmailChange = (e) => {
        this.setState({
            emailValue: e.target.value
        });
    };

    async handleSubmit(e) {
        e.preventDefault();
        await this.setState({
            sentStudent:{
                name: this.state.nameValue,
                email: this.state.emailValue
            }
        });
        await fetch('/api/student', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.sentStudent),
        });
       window.location.reload();
    }

    async deleteStudent(e){
        console.log(e.target.id);
        await fetch('/api/student/' + e.target.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        window.location.reload();
    }




    render() {
        const {students, isLoading, keys} = this.state;
        if (keys[0] === "id") {
            keys.shift();
        }
        if (isLoading) {
            return (<div>The student list is loading...</div>)
        }

        return (
            <div>
                <AppNav modelName='Students'/>
                <InputTxtStudent
                    onChangeName={this.handleNameChange}
                    onChangeEmail={this.handleEmailChange}
                    nameValue={this.state.nameValue}
                    emailValue={this.state.emailValue}/>
                <CoolButton callFunction={this.handleSubmit} name="Add Student"/>
                <Table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        students.map(student =>
                            <tr key={'row' + student.id}>
                                <th key={'column' + student.id} scope="row">{student.id}</th>
                                <td key={'column' + student.name}>{student.name}</td>
                                <td key={'column' + student.email}>{student.email}</td>
                                <td key={'buton' + student.id}><DeleteButton id={student.id} callFunction={this.deleteStudent}/></td>
                            </tr>
                        )
                    }
                    </tbody>
                </Table>
            </div>

        )
    }
}

export default ListOfStudents;