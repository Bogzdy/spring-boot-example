import React, {Component} from 'react';
import {Form, Input, FormGroup, Row, Col, Container, Label} from 'reactstrap';

class InputTxtTeacher extends Component {
    state = {
        values:[]
    };


    render() {
        return (

            <div style={{marginTop: "1px"}}>
                <Container fluid={true}>
                    <Form onSubmit={this.props.submitMethod} style={{width: '50%', margin: 'auto'}}>
                        <Row form>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for='name'>Name:</Label>
                                    <Input
                                        type="text"
                                        onChange={this.props.onChangeName}
                                        value={this.props.nameValue} name='name'
                                        placeholder='Teacher name'/>
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for='email'>E-mail</Label>
                                    <Input type="text" onChange={this.props.onChangeEmail} value={this.props.emailValue} name='email' placeholder='E-mail'/>
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for='phoneNumber'>Phone number</Label>
                                    <Input type="text" onChange={this.props.onChangeEmail} value={this.props.emailValue} name='phoneNumber' placeholder='Phone number'/>
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </Container>
                {this.props.buttonLocation}
            </div>
        );
    }

}

export default InputTxtTeacher;