import React, {Component} from 'react'
import AppNav from "./AppNav";
import {Table} from 'reactstrap';
import DeleteButton from "./DeleteButton";
import InputTxtTeacher from "./InputTxtTeacher";
import CoolButton from "./CoolButton";

class ListOfTeachers extends Component {

    state = {
        isLoading: true,
        teachers: []
    }

    async componentDidMount() {
        const response = await fetch('/api/teacher');
        const body = await response.json();
        this.setState({teachers: body, isLoading: false})
    }

    render() {
        const {teachers, isLoading} = this.state;
        if (isLoading) {
            return (<div>Loading....</div>)
        }
        return (
            <div>
                <AppNav modelName="Teachers"/>
                <InputTxtTeacher/>
                <CoolButton name='Add Teacher'/>
                <Table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Phone Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        teachers.map(teacher =>
                            <tr key={'row' + teacher.id}>
                                <th key={'column1' + teacher.id} scope="row">{teacher.id}</th>
                                <td key={'column2' + teacher.name}>{teacher.name}</td>
                                <td key={'column3' + teacher.email}>{teacher.email}</td>
                                <td key={'column4' + teacher.phoneNumber}>{teacher.phoneNumber}</td>
                                <td key={'buton' + teacher.id}><DeleteButton id={teacher.id} callFunction={this.deleteTeacher}/></td>
                            </tr>
                        )
                    }
                    </tbody>
                </Table>
            </div>
        )
    }

}

export default ListOfTeachers;