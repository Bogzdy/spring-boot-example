import React, {Component} from 'react';
import { Button } from 'reactstrap';

class CoolButton extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        const data = new FormData(e.target);
        fetch('/api/student', {
            method: 'POST',
            body: data
        })
    }

    render() {
        return (
            <div style={{margin: 'auto', width: '50%', textAlign: 'center'}}>
                <Button color="success" onClick={this.props.callFunction}>{this.props.name}</Button>
            </div>
        );
    }

}
export default CoolButton;