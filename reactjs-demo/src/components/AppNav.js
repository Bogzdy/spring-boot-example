import React, {Component} from "react";

import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';


class AppNav extends Component{
    state = { };


    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="md">
                    <NavbarBrand href="/">{this.props.modelName}</NavbarBrand>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/Teachers">Teachers</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/Students">Students</NavLink>
                            </NavItem>
                        </Nav>
                </Navbar>
            </div>
        );
    }
}

export default AppNav;