package com.sda.sdademo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractModel<ID_TYPE> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID_TYPE id;

    @Column(nullable = true, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp created;

}
