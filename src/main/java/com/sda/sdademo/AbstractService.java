package com.sda.sdademo;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<ID_TYPE, MODEL extends AbstractModel, REPOSITORY extends JpaRepository> {

    protected REPOSITORY repository;

    public AbstractService(REPOSITORY repository){
        this.repository = repository;
    }

    public abstract MODEL add(MODEL model);

    public abstract List<MODEL> get();

    public abstract Optional<MODEL> get(ID_TYPE id);

    public abstract void delete(ID_TYPE id) throws EmptyResultDataAccessException;

    public abstract MODEL update(MODEL model);
}
