package com.sda.sdademo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public class AbstractController<ID_TYPE,
                                MODEL extends AbstractModel<ID_TYPE>,
                                REPOSITORY extends JpaRepository,
                                SERVICE extends AbstractService<ID_TYPE, MODEL, REPOSITORY>> {
    protected SERVICE service;


    public AbstractController(SERVICE service) {
        this.service = service;
    }


    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<MODEL> add(@RequestBody MODEL model) {
        MODEL addedModel = service.add(model);
        return ResponseEntity.ok(addedModel);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<MODEL>> getStudent() {
        return ResponseEntity.ok(service.get());
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<MODEL> get(@PathVariable ID_TYPE id) {
        Optional<MODEL> foundModel = service.get(id);
        return foundModel.isPresent() ? ResponseEntity.ok(foundModel.get()) : ResponseEntity.notFound().build();
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity delete(@PathVariable ID_TYPE id)  throws EmptyResultDataAccessException {
        try{
            service.delete(id);
            return ResponseEntity.noContent().build();
        }catch (EmptyResultDataAccessException e){
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<MODEL> update(@RequestBody MODEL model){
        try{
            MODEL updatedModel  = service.update(model);
            return ResponseEntity.ok(updatedModel);
        }catch (IllegalArgumentException e){
            return ResponseEntity.badRequest().build();
        }
    }
}
