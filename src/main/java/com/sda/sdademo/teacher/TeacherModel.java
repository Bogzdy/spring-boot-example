package com.sda.sdademo.teacher;

import com.sda.sdademo.AbstractModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class TeacherModel extends AbstractModel<Long> {

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String phoneNumber;
}
