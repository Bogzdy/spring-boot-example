package com.sda.sdademo.teacher;

import com.sda.sdademo.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/teacher")
public class TeacherController extends AbstractController<Long, TeacherModel, TeacherRepository, TeacherService> {

    @Autowired
    public TeacherController(TeacherService service) {
        super(service);
    }
}
