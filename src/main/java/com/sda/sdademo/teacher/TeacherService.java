package com.sda.sdademo.teacher;

import com.sda.sdademo.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService extends AbstractService<Long, TeacherModel, TeacherRepository> {
    private TeacherRepository teacherRepository;

    @Autowired
    public TeacherService(TeacherRepository teacherRepository) {
        super(teacherRepository);
    }

    @Override
    public TeacherModel add(TeacherModel teacher) {
        if (teacher.getName() == null) {
            teacher.setName("John Doe");
        }
        return repository.saveAndFlush(teacher);
    }

    @Override
    public List<TeacherModel> get() {
        return repository.findAll();
    }

    @Override
    public Optional<TeacherModel> get(Long id) {
        return repository.findById(id);
    }

    @Override
    public void delete(Long id) throws EmptyResultDataAccessException {
        repository.deleteById(id);
    }

    @Override
    public TeacherModel update(TeacherModel teacher) {
        if (teacher.getId() == null) {
            throw new IllegalArgumentException();
        }

        Optional<TeacherModel> existingStudent = repository.findById(teacher.getId());
        if (existingStudent.isPresent()){
            return repository.saveAndFlush(teacher);
        }else {
            throw new IllegalArgumentException();
        }
    }
}
