package com.sda.sdademo.student;

import com.sda.sdademo.AbstractModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@Entity
public class StudentModel extends AbstractModel<Long> {

    @Column
    private String name;

    @Column
    private String email;

}
