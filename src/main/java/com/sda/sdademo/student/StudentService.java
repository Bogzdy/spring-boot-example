package com.sda.sdademo.student;

import com.sda.sdademo.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService extends AbstractService<Long, StudentModel, StudentRepository>{


    @Autowired
    public StudentService(StudentRepository studentRepository) {
        super(studentRepository);
    }

    @Override
    public StudentModel add(StudentModel student) {
        if (student.getName() == null) {
            student.setName("Anonim");
        }
        return repository.saveAndFlush(student);
    }

    @Override
    public List<StudentModel> get() {
        return repository.findAll();
    }

    @Override
    public Optional<StudentModel> get(Long id) {
        return repository.findById(id);
    }

    @Override
    public void delete(Long id) throws EmptyResultDataAccessException {
        repository.deleteById(id);
    }

    @Override
    public StudentModel update(StudentModel studentToUpdate) {
        if (studentToUpdate.getId() == null){
            throw new IllegalArgumentException();
        }
        Optional<StudentModel> existingStudent = repository.findById(studentToUpdate.getId());
        if (existingStudent.isPresent()){
            return repository.saveAndFlush(studentToUpdate);
        }else {
            throw new IllegalArgumentException();
        }
    }
}
