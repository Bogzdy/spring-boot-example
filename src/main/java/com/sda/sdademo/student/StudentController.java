package com.sda.sdademo.student;

import com.sda.sdademo.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/student")
public class StudentController  extends AbstractController<Long, StudentModel, StudentRepository, StudentService> {

    @Autowired
    public StudentController(StudentService service) {
        super(service);
    }
}
